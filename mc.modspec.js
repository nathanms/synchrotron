{
    "name": "net.lshift.synchrotron.Mc",
    "imports": ["net.lshift.synchrotron.Graph",
		"net.lshift.synchrotron.Diff",
		"org.JSON",
		"uk.org.pajhome.SHA1"],
    "exports": [{"Util": "Mc.Util"},
		{"ObjectTypes": "Mc.ObjectTypes"},
		{"TypeDirectory": "Mc.TypeDirectory"},
		{"validInstance": "Mc.validInstance"},
		{"lookupType": "Mc.lookupType"},
		{"typeMethod": "Mc.typeMethod"},
		{"SimpleObjectTypeTable": "Mc.SimpleObjectTypeTable"},
		{"typeTableFun": "Mc.typeTableFun"},
		{"Repository": "Mc.Repository"},
		{"Checkout": "Mc.Checkout"}]
}
